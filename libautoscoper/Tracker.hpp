// ----------------------------------
// Copyright (c) 2011, Brown University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// (1) Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//
// (2) Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// (3) Neither the name of Brown University nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY BROWN UNIVERSITY “AS IS” WITH NO
// WARRANTIES OR REPRESENTATIONS OF ANY KIND WHATSOEVER EITHER EXPRESS OR
// IMPLIED, INCLUDING WITHOUT LIMITATION ANY WARRANTY OF DESIGN OR
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, EACH OF WHICH ARE
// SPECIFICALLY DISCLAIMED, NOR ANY WARRANTY OR REPRESENTATIONS THAT THE
// SOFTWARE IS ERROR FREE OR THAT THE SOFTWARE WILL NOT INFRINGE ANY
// PATENT, COPYRIGHT, TRADEMARK, OR OTHER THIRD PARTY PROPRIETARY RIGHTS.
// IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY OR CAUSE OF ACTION, WHETHER IN CONTRACT,
// STRICT LIABILITY, TORT, NEGLIGENCE OR OTHERWISE, ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. ANY RECIPIENT OR USER OF THIS SOFTWARE ACKNOWLEDGES THE
// FOREGOING, AND ACCEPTS ALL RISKS AND LIABILITIES THAT MAY ARISE FROM
// THEIR USE OF THE SOFTWARE.
// ---------------------------------

/// \file Tracker.hpp
/// \author Andy Loomis

#ifndef XROMM_TRACKER_H
#define XROMM_TRACKER_H

#include <vector>
#include <string>

#include "RayCaster.hpp"
#include "RadRenderer.hpp"
#include "Trial.hpp"
#include "OpenCL.hpp"

namespace xromm
{

class Camera;
class CoordFrame;

namespace opencl
{

class Filter;
class View;
class VolumeDescription;

} // namespace opencl

class Tracker
{
public:

    Tracker();
    ~Tracker();
    void load(const Trial& trial);
    Trial* trial() { return &trial_; }
    void optimize(int frame, int dframe, int repeats = 1);
    double minimizationFunc(const double* values) const;
    std::vector<opencl::View*>& views() { return views_; }
    const std::vector<opencl::View*>& views() const { return views_; }
    opencl::View* view(size_t i) { return views_.at(i); }
    const opencl::View* view(size_t i) const { return views_.at(i); }

private:
    void calculate_viewport(const CoordFrame& modelview, double* viewport) const;

    Trial trial_;
    opencl::VolumeDescription* volumeDescription_;
    std::vector<opencl::View*> views_;
	opencl::Buffer* rendered_drr_;
	opencl::Buffer* rendered_rad_;
};

} // namespace XROMM

#endif // XROMM_TRACKER_H
