outfile "autoscoper-1.12-win32-setup.exe"

# Set the default installation directory
installDir $PROGRAMFILES\Autoscoper-1.12

# Prompt the user for another directory
dirText "This will install the Autoscoper-1.12 on your computer."

Section -Prerequisites
	setOutPath $INSTDIR\Prerequisites
	MessageBox MB_YESNO "Do you want to install the required GTK?" /SD IDYES IDNO endGTK
    		File Prerequisites\gtk-2.12.9-win32-2.exe
    		ExecWait $INSTDIR\Prerequisites\gtk-2.12.9-win32-2.exe
  	endGTK:
  	MessageBox MB_YESNO "Do you want to install the required Microsoft Visual C++ 2010 Redistributable?" /SD IDYES IDNO endVCRedist
    		File Prerequisites\vcredist_x86.exe
    		ExecWait $INSTDIR\Prerequisites\vcredist_x86.exe
  	endVCRedist:
SectionEnd


# Default installation section
section

	# Set the installation directory
	setOutPath $INSTDIR

	# Create the uninstaller
	writeUninstaller $INSTDIR\uninstall.exe
	
	# Install the application and dlls
	file autoscoper-1.12.exe
	file glut32.dll
	file glew32.dll
	file cudart32_41_28.dll
	file new_trial_dialog.glade

	# Create a desktop shortcut
	createShortCut "$DESKTOP\Autoscoper-1.12.lnk" "$INSTDIR\autoscoper-1.12.exe"
	
sectionEnd


# Uninstaller section
section "Uninstall"

	# Remove the uninstaller
	delete $INSTDIR\uninstall.exe
	
	# Remove the installed files
	delete $INSTDIR\autoscoper-1.12.exe
	delete $INSTDIR\glut32.dll
	delete $INSTDIR\glew32.dll
	delete $INSTDIR\cudart32_41_28.dll
	delete $INSTDIR\new_trial_dialog.glade
	delete $INSTDIR\Prerequisites\vcredist_x86.exe
	delete $INSTDIR\Prerequisites\gtk-2.12.9-win32-2.exe

	# Remove the installation directory
	RMDir $INSTDIR\Prerequisites
	RMDir $INSTDIR

	# Remove the desktop shortcut
	delete $DESKTOP\Autoscoper-1.10.lnk"


sectionEnd