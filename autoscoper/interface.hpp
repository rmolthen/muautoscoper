/*
 * DO NOT EDIT THIS FILE - it is generated by Glade.
 */

GtkWidget* create_xromm_markerless_tracking_window (void);
GtkWidget* create_xromm_markerless_tracking_tracking_dialog (void);
GtkWidget* create_xromm_drr_renderer_properties_dialog (void);
GtkWidget* create_xromm_sobel_properties_dialog (void);
GtkWidget* create_xromm_contrast_properties_dialog (void);
GtkWidget* create_xromm_gaussian_properties_dialog (void);
GtkWidget* create_xromm_sharpen_properties_dialog (void);
GtkWidget* create_export_tracking_options_dialog (void);
GtkWidget* create_frame_range_dialog (void);
GtkWidget* create_frame_range_dialog2 (void);
